package au.edu.unimelb.cs.culnane.cryptoml;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;

import au.edu.unimelb.cs.culnane.crypto.P_Length;
import au.edu.unimelb.cs.culnane.crypto.ZpStarSafePrimeGroupRFC3526;
import au.edu.unimelb.cs.culnane.crypto.elgamal.ElGamal;
import au.edu.unimelb.cs.culnane.crypto.elgamal.ElGamalCipher;
import au.edu.unimelb.cs.culnane.crypto.elgamal.ElGamalPublicKey;
import au.edu.unimelb.cs.culnane.crypto.exceptions.CryptoException;
import au.edu.unimelb.cs.culnane.crypto.exceptions.GroupException;
import au.edu.unimelb.cs.culnane.storage.StorageObject;
import au.edu.unimelb.cs.culnane.storage.exceptions.StorageException;
import au.edu.unimelb.cs.culnane.storage.json.JSONStorageObject;

public class Encrypt {
	private ElGamalPublicKey kp;
	private ZpStarSafePrimeGroupRFC3526 group = new ZpStarSafePrimeGroupRFC3526();
	private JSONStorageObject jso = new JSONStorageObject();

	public Encrypt() throws StorageException, GroupException {

		kp = new ElGamalPublicKey(jso.readFromFile("jointPublicKey.json"));
		group.initialise(P_Length.P2046);

	}

	public String encrypt(JSONArray messages, boolean exponential) throws CryptoException, IOException {
		List<StorageObject<JSONStorageObject>> jsonStores = new ArrayList<StorageObject<JSONStorageObject>>();
		for (int i = 0; i < messages.length(); i++) {
			BigInteger m = new BigInteger(messages.getString(i));
			if(exponential){
				m = group.get_g().modPow(m, group.get_p());
			}
			ElGamalCipher cipher = ElGamal.encrypt(group, kp, m);
			JSONStorageObject cipherObj = jso.getNewStorageObject();
			cipher.storeInStorageObject(cipherObj);
			jsonStores.add(cipherObj);
		}
		JSONStorageObject outObj = jso.getNewStorageObject();
		outObj.set("ciphers", jsonStores);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(out));
		outObj.writeToStream(bw);
		bw.flush();
		return new String(out.toByteArray());
	}

	public String encrypt(String message, boolean exponential) throws CryptoException, IOException {

		BigInteger m = new BigInteger(message);
		if(exponential){
			m = group.get_g().modPow(m, group.get_p());
		}
		ElGamalCipher cipher = ElGamal.encrypt(group, kp, m);
		JSONStorageObject cipherObj = jso.getNewStorageObject();
		cipher.storeInStorageObject(cipherObj);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(out));
		cipherObj.writeToStream(bw);
		bw.flush();
		return new String(out.toByteArray());
	}

	public String multiplyCiphers(String ciphersStorageObj) throws StorageException {
		JSONStorageObject cipherObject = new JSONStorageObject(ciphersStorageObj);
		List<StorageObject<JSONStorageObject>> ciphers = cipherObject.getList("ciphers");
		ElGamalCipher sum=null;
		
		for(StorageObject<JSONStorageObject> cipher:ciphers){
			if(sum==null){
				sum = new ElGamalCipher(cipher);
			}else{
				sum.multiplyCipher(new ElGamalCipher(cipher),group.get_p());
			}
		}
		JSONStorageObject output = cipherObject.getNewStorageObject();
		return sum.storeInStorageObject(output).toString();
	}
	public BigInteger getExponentialValue(BigInteger value){
		
		return group.get_g().modPow(value, group.get_p());
	}

	public static void main(String[] args) throws GroupException, CryptoException, IOException {
		Encrypt enc = new Encrypt();
		JSONArray test = new JSONArray();
		test.put("1234");
		test.put("2222");
		test.put("1234");
		String output = enc.encrypt(test,true);
		System.out.println();
		System.out.println(enc.multiplyCiphers(output));
		// JSONStorageObject cipherObject = jso.getNewStorageObject();

	}
}
