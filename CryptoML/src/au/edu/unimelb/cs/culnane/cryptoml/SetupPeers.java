package au.edu.unimelb.cs.culnane.cryptoml;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.json.JSONArray;
import org.json.JSONObject;

import au.edu.unimelb.cs.culnane.crypto.elgamal.ElGamalPublicKey;
import au.edu.unimelb.cs.culnane.protocols.ProtocolExecutionException;
import au.edu.unimelb.cs.culnane.protocols.distkeygen.DistKeyGenRunner;
import au.edu.unimelb.cs.culnane.protocols.runners.SocketProtocolRunnerException;
import au.edu.unimelb.cs.culnane.storage.json.JSONStorageObject;
import au.edu.unimelb.cs.culnane.storage.stores.HashStore;
import au.edu.unimelb.cs.culnane.utils.IOUtils;

public class SetupPeers {

	private static void generatePeersJSON(int peerCount) throws IOException{
		JSONArray output = new JSONArray();
		for (int i = 0; i < peerCount; i++) {
			JSONObject peer = new JSONObject();
			peer.put("id", "Peer" + (i+1));
			peer.put("host","localhost");
			peer.put("port", 8091 + i);
			output.put(peer);
		}
		IOUtils.writeToFile("./peers.json", output);
	}
	
	public void create(int peerCount, int threshold) throws IOException, SocketProtocolRunnerException, InterruptedException{
		
		File peers = new File("./peers.json");
		if (!peers.exists()) {
			generatePeersJSON(peerCount);
		} else {
			JSONArray peerArr =IOUtils.readJSONArrayFromFile("./peers.json");
			if(peerArr.length()!=peerCount){
				System.out.println("Invalid peers.json, will regenerate");
				generatePeersJSON(peerCount);
			}else{
				System.out.println("Found valid peers.json, will use it");
			}
			
		}

		List<HashStore<JSONStorageObject>> storage = new ArrayList<HashStore<JSONStorageObject>>();
		ExecutorService exec = Executors.newFixedThreadPool(peerCount);
		for (int i = 0; i < peerCount; i++) {
			String peerID = "Peer" + (i + 1);
			storage.add(new HashStore<JSONStorageObject>(new JSONStorageObject()));
			DistKeyGenRunner distKeyRunner = new DistKeyGenRunner(peerID, (8091 + i), "./peers.json", true,
					storage.get(storage.size() - 1),threshold);
			exec.execute(new Runnable() {
				public void run() {
					try {
						distKeyRunner.runProtocol();
					} catch (ProtocolExecutionException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			});
		}
		exec.shutdown();
		while (!exec.awaitTermination(500, TimeUnit.MILLISECONDS)) {

		}
		for (int i = 0; i < storage.size(); i++) {
			storage.get(i).writeStorageToFile("./" + "Peer" + (i + 1) + "storage.txt");
		}
		JSONStorageObject jso = new JSONStorageObject();
		ElGamalPublicKey pubKey = new ElGamalPublicKey(jso.readFromFile("./peer1.js"));
		pubKey.storeInStorageObject(jso);
		jso.writeToFile("jointPublicKey.json");
		
	}
	public void closeWithException() throws Exception{
		throw new Exception("Application Shutdown");
	}
	public static void main(String[] args)
			throws SocketProtocolRunnerException, ProtocolExecutionException, IOException, InterruptedException {
		// TODO Auto-generated method stub
		
		//ByteArrayOutputStream baos = new ByteArrayOutputStream();
		//FileOutputStream fos =new FileOutputStream("./templog.txt");
		//System.setOut(new PrintStream(fos));
		
		int peerCount =4;
		if (args.length > 0) {
			peerCount = Integer.parseInt(args[0]);
		}
		SetupPeers setup = new SetupPeers();
		long start = System.nanoTime();
		setup.create(peerCount,3);
		long end = System.nanoTime();
		System.out.println("Setup:" +TimeUnit.NANOSECONDS.toMillis(end-start));
		
	}

}
