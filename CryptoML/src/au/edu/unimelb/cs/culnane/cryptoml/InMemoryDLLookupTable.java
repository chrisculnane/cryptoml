package au.edu.unimelb.cs.culnane.cryptoml;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import au.edu.unimelb.cs.culnane.crypto.P_Length;
import au.edu.unimelb.cs.culnane.crypto.ZpStarSafePrimeGroupRFC3526;
import au.edu.unimelb.cs.culnane.crypto.exceptions.GroupException;

public class InMemoryDLLookupTable {

	private Map<BigInteger,Integer> map = new HashMap<BigInteger,Integer>();
	private ZpStarSafePrimeGroupRFC3526 group;
	public InMemoryDLLookupTable(ZpStarSafePrimeGroupRFC3526 group){
		this.group=group;
		
	}
	public void construct(int min, int max){
		map.clear();
		for(int i=min;i<=max;i++){
			map.put(group.get_g().modPow(BigInteger.valueOf(i), group.get_p()),i);
		}
		System.out.println(map.size());
	}
	
	public int lookup(BigInteger plaintext){
		return map.get(plaintext);
	}
	public static void main(String[] args) throws GroupException{
		ZpStarSafePrimeGroupRFC3526 group = new ZpStarSafePrimeGroupRFC3526();
		group.initialise(P_Length.P2046);
		InMemoryDLLookupTable lookup= new InMemoryDLLookupTable(group);
		long start = System.nanoTime();
		lookup.construct(-1000, 10000);
		long end = System.nanoTime();
		System.out.println(TimeUnit.MILLISECONDS.convert((Integer.MAX_VALUE/100000)*(end-start),TimeUnit.NANOSECONDS));
		
		
	}
}
