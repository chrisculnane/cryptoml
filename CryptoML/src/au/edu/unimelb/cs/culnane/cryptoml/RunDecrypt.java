package au.edu.unimelb.cs.culnane.cryptoml;

import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.json.JSONArray;

import au.edu.unimelb.cs.culnane.crypto.P_Length;
import au.edu.unimelb.cs.culnane.crypto.ZpStarSafePrimeGroupRFC3526;
import au.edu.unimelb.cs.culnane.crypto.exceptions.CryptoException;
import au.edu.unimelb.cs.culnane.crypto.exceptions.GroupException;
import au.edu.unimelb.cs.culnane.protocols.ProtocolExecutionException;
import au.edu.unimelb.cs.culnane.protocols.runners.SocketProtocolRunnerException;
import au.edu.unimelb.cs.culnane.protocols.threshold.decryption.ThresholdDecryptionRunner;
import au.edu.unimelb.cs.culnane.storage.exceptions.StorageException;
import au.edu.unimelb.cs.culnane.storage.json.JSONStorageObject;
import au.edu.unimelb.cs.culnane.storage.stores.HashStore;

public class RunDecrypt {

	private int peerCount;
	private int threshold;
	private List<HashStore<JSONStorageObject>> storage = new ArrayList<HashStore<JSONStorageObject>>();
	private List<ThresholdDecryptionRunner> runners = new ArrayList<ThresholdDecryptionRunner>();

	public RunDecrypt(int peerCount, int threshold) throws SocketProtocolRunnerException, GroupException, IOException {
		this.peerCount = peerCount;
		this.threshold = threshold;
		for (int i = 0; i < peerCount; i++) {

			String peerID = "Peer" + (i + 1);
			storage.add(new HashStore<JSONStorageObject>(new JSONStorageObject()));

			ThresholdDecryptionRunner thresholdDecrypt = new ThresholdDecryptionRunner(peerID, (8091 + i),
					"./peers.json", true, storage.get(storage.size() - 1));
			runners.add(thresholdDecrypt);
		}

	}

	public JSONArray decrypt(JSONArray messages)
			throws SocketProtocolRunnerException, GroupException, IOException, InterruptedException {
		JSONArray output = new JSONArray();
		for (int i = 0; i < messages.length(); i++) {
			output.put(decrypt(messages.getJSONObject(i).toString()));
		}
		return output;
	}

	public String decrypt(String message)
			throws SocketProtocolRunnerException, GroupException, IOException, InterruptedException {

		ExecutorService exec = Executors.newFixedThreadPool(peerCount);
		UUID uuid = UUID.randomUUID();

		JSONStorageObject cipher = new JSONStorageObject(message);
		for (ThresholdDecryptionRunner runner : runners) {
			exec.execute(new Runnable() {
				public void run() {
					try {
						runner.removeProtocol();
						runner.doDecryption(uuid.toString(), threshold, cipher);
					} catch (ProtocolExecutionException | StorageException | GroupException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			});
		}

		exec.shutdown();
		while (!exec.awaitTermination(500, TimeUnit.MILLISECONDS)) {

		}
		JSONArray results = new JSONArray();
		for (int i = 0; i < storage.size(); i++) {
			storage.get(i).writeStorageToFile("./" + "Peer" + (i + 1) + "storage.txt");
			results.put(storage.get(i).get("ThresholdDecryptionProtocol.decryption").getString("plaintext"));
		}

		return results.toString();
		// System.exit(0);

	}

	public void closeWithException() throws Exception {
		throw new Exception("Application Shutdown");
	}

	public static void main(String[] args)
			throws SocketProtocolRunnerException, GroupException, IOException, InterruptedException, CryptoException {
		Encrypt enc = new Encrypt();
		JSONArray test = new JSONArray();
		test.put("1234");
		test.put("2222");
		test.put("5555");
		String output=enc.encrypt(test,true);
		String sumOfCiphers = enc.multiplyCiphers(output);
		//JSONObject cipherObj = new JSONObject(enc.encrypt(test,true));
		//JSONArray ciphers = cipherObj.getJSONArray("ciphers");

		// System.out.println(o);

		//String sample = "{\"c1\":\"a52517267eddcb35167d50dec777b9c063f235cd8df8182f11cfac3aec44ab6a756d4cbf5364af8325668e7bf995b8e93f4c37616d22cdf52af9206e932be4e9681fe5482897d153cd67228fb290a1dd32c28721fc3003d53f040697366522769e03b6ea49db17fa95c126e117566ce2552f8a60de8d5a072f472f1f6e5ace88ace273e158788453a73c503b98c9d06e50d1498ef78fe349469101d28b71fb21557f32a3f450e04a7ff816aaa76ef47c58813e0f0618761d54626e74f3eea5f0c264e72ee22a8b6fe325cf90203dbc60ef0d550cd2089aa8c5bb14799c3117867ed534e9b90687b537fd911ba5c04202ce88cb26dd56c9f8708eb95713e6fc5a\",\"c2\":\"a8f76c0f5fbb27e3cfa0a3d72d04b7cb5dda5a2c305d056d59748c3a7de9e43d211fdf7df7757d0e89453a5dd21f119a5e4a232b46c667317679a01d40d4d06b20ff7f9a322d5528e82d20f414866c7e20baf3b9ae84b02ea0e240d8f9b6a8c297905f97ecb72d7675ffed4a8fb813e26b207e7e5ab2e01f81d01c22e49b86350ec93da53920318e761944e5f2ed2602526e05326b0eaea07c210c73a847f0e281e536b81275430f732d85ab6fd4b7515fe18ffb75150fcc32bb1c7c70898e667dbb2d24f6f9f5b0a5e6efb864da6f3b4c5dffdaabeac2514856506107289fef7af403e60a545b6b6060bc35fe047e13bfe69ba54157b5a06eea6227e2dd2b15\"}";
		RunDecrypt dec = new RunDecrypt(4, 3);
		long start = System.nanoTime();
		JSONArray decryptions = new JSONArray(dec.decrypt(sumOfCiphers));
		BigInteger decryptionFrom1stPeer = new BigInteger(decryptions.getString(0));
		long end = System.nanoTime();
		System.out.println("Decryption:" + TimeUnit.NANOSECONDS.toMillis(end-start));
		
		//System.out.println(decryptions.getString(0));
		//System.out.println("finishedFirst");
		ZpStarSafePrimeGroupRFC3526 group = new ZpStarSafePrimeGroupRFC3526();
		group.initialise(P_Length.P2046);
		System.out.println("Starting to build lookup table");
		InMemoryDLLookupTable lookup= new InMemoryDLLookupTable(group);
		lookup.construct(-100000, 100000);
		System.out.println("Finished building table");
		start = System.nanoTime();
		System.out.println(lookup.lookup(decryptionFrom1stPeer));
		end = System.nanoTime();
		System.out.println("Lookup:" + TimeUnit.NANOSECONDS.toMillis(end-start));
		//13712509917723016379923717338223577885624421737432673185006398894145923296791929193202696907530080567641674309282095788929564900026217863549432606220480091752871104021944667257830131830357449549866312624448767344466240697225647034139690932863848044754354581389806906313164044287793971442850660328747945893391563806261692111183702002546501871703438447342895273123781932225594994860328590431653440012804452788151100778176909269710226403932992740917140874157082782475221730613341816135269447341297250579343193377466448207198632603831813588892371572825827094571494211779955663841256006273686386784224822879922675700837137
		//System.out.println(enc.getExponentialValue(new BigInteger("9011")));//Sum of g^(1234+2222+5555)
		
		// System.in.read();
		// dec.decrypt(o);
		//3383563832
	}
}
